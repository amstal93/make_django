<?xml version="1.0" encoding="UTF-8"?>
<ui version="4.0">
 <class>MainUi</class>
 <widget class="QMainWindow" name="MainUi">
  <property name="geometry">
   <rect>
    <x>0</x>
    <y>0</y>
    <width>900</width>
    <height>600</height>
   </rect>
  </property>
  <property name="palette">
   <palette>
    <active>
     <colorrole role="WindowText">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>255</red>
        <green>255</green>
        <blue>255</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Button">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>46</red>
        <green>52</green>
        <blue>54</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Light">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>69</red>
        <green>78</green>
        <blue>81</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Midlight">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>57</red>
        <green>65</green>
        <blue>67</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Dark">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>23</red>
        <green>26</green>
        <blue>27</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Mid">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>31</red>
        <green>35</green>
        <blue>36</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Text">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>255</red>
        <green>255</green>
        <blue>255</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="BrightText">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>255</red>
        <green>255</green>
        <blue>255</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="ButtonText">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>255</red>
        <green>255</green>
        <blue>255</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Base">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Window">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>46</red>
        <green>52</green>
        <blue>54</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Shadow">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="AlternateBase">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>23</red>
        <green>26</green>
        <blue>27</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="ToolTipBase">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>255</red>
        <green>255</green>
        <blue>220</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="ToolTipText">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
       </color>
      </brush>
     </colorrole>
    </active>
    <inactive>
     <colorrole role="WindowText">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Button">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>239</red>
        <green>239</green>
        <blue>239</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Light">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>255</red>
        <green>255</green>
        <blue>255</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Midlight">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>202</red>
        <green>202</green>
        <blue>202</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Dark">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>159</red>
        <green>159</green>
        <blue>159</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Mid">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>184</red>
        <green>184</green>
        <blue>184</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Text">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="BrightText">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>255</red>
        <green>255</green>
        <blue>255</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="ButtonText">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Base">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>255</red>
        <green>255</green>
        <blue>255</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Window">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>239</red>
        <green>239</green>
        <blue>239</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Shadow">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>118</red>
        <green>118</green>
        <blue>118</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="AlternateBase">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>247</red>
        <green>247</green>
        <blue>247</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="ToolTipBase">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>255</red>
        <green>255</green>
        <blue>220</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="ToolTipText">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
       </color>
      </brush>
     </colorrole>
    </inactive>
    <disabled>
     <colorrole role="WindowText">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>23</red>
        <green>26</green>
        <blue>27</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Button">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>46</red>
        <green>52</green>
        <blue>54</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Light">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>69</red>
        <green>78</green>
        <blue>81</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Midlight">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>57</red>
        <green>65</green>
        <blue>67</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Dark">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>23</red>
        <green>26</green>
        <blue>27</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Mid">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>31</red>
        <green>35</green>
        <blue>36</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Text">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>23</red>
        <green>26</green>
        <blue>27</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="BrightText">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>255</red>
        <green>255</green>
        <blue>255</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="ButtonText">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>23</red>
        <green>26</green>
        <blue>27</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Base">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>46</red>
        <green>52</green>
        <blue>54</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Window">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>46</red>
        <green>52</green>
        <blue>54</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="Shadow">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>177</red>
        <green>177</green>
        <blue>177</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="AlternateBase">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>247</red>
        <green>247</green>
        <blue>247</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="ToolTipBase">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>255</red>
        <green>255</green>
        <blue>220</blue>
       </color>
      </brush>
     </colorrole>
     <colorrole role="ToolTipText">
      <brush brushstyle="SolidPattern">
       <color alpha="255">
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
       </color>
      </brush>
     </colorrole>
    </disabled>
   </palette>
  </property>
  <property name="windowTitle">
   <string>Django Maker</string>
  </property>
  <widget class="QWidget" name="centralwidget">
   <property name="enabled">
    <bool>true</bool>
   </property>
   <property name="minimumSize">
    <size>
     <width>900</width>
     <height>600</height>
    </size>
   </property>
   <property name="autoFillBackground">
    <bool>true</bool>
   </property>
   <widget class="QWidget" name="formLayoutWidget">
    <property name="geometry">
     <rect>
      <x>0</x>
      <y>0</y>
      <width>801</width>
      <height>591</height>
     </rect>
    </property>
    <layout class="QFormLayout" name="formLayout">
     <item row="0" column="0">
      <widget class="QLabel" name="nomeAPPLabel">
       <property name="text">
        <string>Nome APP</string>
       </property>
      </widget>
     </item>
     <item row="0" column="1">
      <widget class="QLineEdit" name="nome_input"/>
     </item>
     <item row="1" column="0">
      <widget class="QLabel" name="portaAPPLabel">
       <property name="text">
        <string>Porta APP</string>
       </property>
      </widget>
     </item>
     <item row="1" column="1">
      <widget class="QLineEdit" name="porta_input"/>
     </item>
     <item row="2" column="0">
      <widget class="QLabel" name="bancoDeDadosPortaLabel">
       <property name="text">
        <string>Banco de dados porta</string>
       </property>
      </widget>
     </item>
     <item row="2" column="1">
      <widget class="QLineEdit" name="bd_porta_input"/>
     </item>
     <item row="3" column="0">
      <widget class="QLabel" name="pythonVersOLabel">
       <property name="text">
        <string>Python Versão</string>
       </property>
      </widget>
     </item>
     <item row="3" column="1">
      <widget class="QLineEdit" name="python_input"/>
     </item>
     <item row="4" column="0">
      <widget class="QLabel" name="postgresUsuRioLabel">
       <property name="text">
        <string>Postgres Usuário</string>
       </property>
      </widget>
     </item>
     <item row="4" column="1">
      <widget class="QLineEdit" name="user_input"/>
     </item>
     <item row="5" column="0">
      <widget class="QLabel" name="postgresSenhaLabel">
       <property name="text">
        <string>Postgres Senha</string>
       </property>
      </widget>
     </item>
     <item row="5" column="1">
      <widget class="QLineEdit" name="password_input"/>
     </item>
     <item row="6" column="0">
      <widget class="QLabel" name="postgresHostLabel">
       <property name="text">
        <string>Postgres Host</string>
       </property>
      </widget>
     </item>
     <item row="6" column="1">
      <widget class="QLineEdit" name="bd_input"/>
     </item>
     <item row="7" column="0">
      <widget class="QLabel" name="caminhoDoAPPLabel">
       <property name="text">
        <string>Caminho do APP</string>
       </property>
      </widget>
     </item>
     <item row="7" column="1">
      <widget class="QLineEdit" name="path_input"/>
     </item>
     <item row="8" column="1">
      <widget class="QPushButton" name="submitButton">
       <property name="text">
        <string>Submit</string>
       </property>
      </widget>
     </item>
    </layout>
   </widget>
  </widget>
 </widget>
 <resources/>
 <connections/>
</ui>
