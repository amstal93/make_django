## Projeto micro serviço

Projeto para criar micro serviços de forma mais otimizada. Para isso tem que
 ter certeza que todas as dependências do da biblioteca PySide6 estejam cumpridas.

[PySimpleGui](https://pypi.org/project/PySimpleGUI/)

Tem que instalar na mão o fork do django responsável pela aplicação

`pip install git+https://github.com/victorhdcoelho/django.git`

Para utilizar a biblioteca basta baixar com pip:

`pip install microservice-maker`

Para rodar a biblioteca basta escrever

`python3 -m make-microservice` ou

`make-microservice`

Siga o passo a passo e no final terá o que deseja
